<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="crud/css/bootstrap.min.css">
</head>
<body>

<form action="test.php" method="post" enctype="multipart/form-data">

<div class="container">
	<div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 ">
			 <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="text" class="form-control" name="username">
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="Password" class="form-control" name="pwd">
  </div>
  <div class="form-check">
    <label class="form-check-label">
      <input type="checkbox" class="form-check-input" name="skill[]" value="HTML">
      HTML
    </label>
  </div>

  <div class="form-check">
    <label class="form-check-label">
      <input type="checkbox" class="form-check-input" name="skill[]" value="CSS">
      CSS
    </label>
  </div>

  <div class="form-check">
    <label class="form-check-label">
      <input type="checkbox" class="form-check-input" name="skill[]" value="PHP">
      PHP
    </label>
  </div>

            <div class="form-group">
                <label class="custom-file">
                    <input type="file" id="file" class="custom-file-input" name="image">
                    <span class="custom-file-control"></span>
                </label>
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
		</div>
    </div>
</div>
 
</form>




<script src="crud/js/bootstrap.js"></script>


</body>
</html>