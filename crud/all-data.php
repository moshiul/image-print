<?php
$db=new PDO('mysql:host=localhost;dbname=Bitm;charset=utf8mb4','root','');

$query="SELECT * FROM `reg_info`";
$stmt=$db->query($query);
$data=$stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<html>
<head>
    <title>Crud Operation</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>

<body>
    <div class="container ">
        <table class="table table-bordered table-responsive">
            <thead >
            <tr >
                <th align="center">Serial No</th>
                <th>Name</th>



                <th>Image</th>

                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $sl=0;
            foreach ($data as $reg_info)
            {
                $sl++;
                ?>
                <tr>
                    <td align="center"><?php echo $sl;?></td>
                    <td><?php echo $reg_info['name'];?></td>
                    <td align="center"><img src="images/<?php echo $reg_info['image']?>" alt="Smiley face" height="142" width="142"></td>

                    <td>
                        <a href="view.php?id=<?php echo $reg_info['id'];?>">View</a>
                        <a href="edit.php?id=<?php echo $reg_info['id'];?>">Edit</a>
                        <a href="delete.php?id=<?php echo $reg_info['id'];?>">Delete</a>

                    </td>

                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</body>
</html>
